import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
from utils import find_2D_error_components
import utils as ut
import matplotlib.pyplot as plt


def main():
    print("test")
    t = np.arange(0, 36, 5)
    error = np.array([0, 0, 0, 0, 0, 0, 0, 0])

    ref_vec = np.array([[2.1], [4.41]])
    pos_vec = np.array([[2], [10]])
    theta = 1.3423

    x_error, y_error = find_2D_error_components(ref_vec, pos_vec, theta)
    # plt.scatter()

    print(x_error)
    print(y_error)

    # plt.plot(t, error, label="$x$ error")
    # plt.plot(t, error, label="$y$ error")
    # plt.plot(t, error, label="$\psi$ error")
    # plt.plot(t, error, label="$b_v$ error")
    # plt.plot(t, error, label="$b_\psi$ error")
    # plt.legend()
    # path = "tikz/"
    # tikz_save(path + "no_noise_error.tex",
    #           figurewidth='\\linewidth',
    #           figureheight='.5\\linewidth'
    #           )
    # plt.show()


if __name__ == '__main__':
    main()
