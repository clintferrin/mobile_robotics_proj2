#! /usr/bin/env python
# coding=utf-8

import numpy as np
from numpy import cos, sin, tan, diag, array
import Parameters as pm
import VehicleSim as ve
import SensorSuite as ss
from utils import rotation_mat


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    sensors = ss.SensorSuite(params)

    # initialize kalman filter with measurements
    y_tilde = [sensors.get_velocity(sim.x),
               sensors.get_steering_angle(sim.x)]

    z_tilde = sensors.get_gps_pos(sim.x)
    observer = KalmanFilter(sensors, y_tilde, z_tilde, params)

    observer.propagate(y_tilde)
    observer.update(z_tilde)

    # observer.P = np.ones((5, 5))
    # x_hat = np.ones((5, 1))
    # y_tilde = [.5, .5]
    # print(observer.P_dot(observer.P, x_hat, y_tilde))
    # print(observer.x_hat_dot(x_hat,y_tilde))


class KalmanFilter:
    def __init__(self, sensors, params):
        # obtain matrix information from sensors
        self.Q = sensors.Q
        self.R = sensors.R
        self.K = []
        self.I = np.eye(5)

        # obtain parameter information
        self.dt = params.dt
        self.tau_v = params.tau_v
        self.tau_phi = params.tau_phi
        self.L = float(params.wheel_base)
        self.gps_ref = params.gps_distance_from_back_axel

        # inital heading
        heading = params.x0[2] + np.random.normal() * params.psi_sig_init

        if not params.noise:
            heading = params.x0[2]

        # calculate initial estimate from gps measurement
        pos = sensors.get_gps_pos(np.array(params.x0).reshape(-1, 1)) \
              - rotation_mat(heading).dot(
            np.array([[self.gps_ref], [0]]))

        # estimate states from measurements
        self.x_hat = array([[pos[0, 0]],
                            [pos[1, 0]],
                            [heading],
                            [0],
                            [0]])
        self.x0 = self.x_hat
        self.distance_traveled = 0

        # build H matrix for discrete sensors
        self.H = np.array([[1, 0, -self.L * sin(self.x_hat[2]), 0, 0],
                           [0, 1, self.L * cos(self.x_hat[2]), 0, 0]])

        # build initial state covariance matrix
        self.P = diag([params.x1_sig_init ** 2,
                       params.x2_sig_init ** 2,
                       params.psi_sig_init ** 2,
                       params.v_b_sig_init ** 2,
                       params.phi_b_sig_init ** 2])

        # update residual covariance H*P*H'+ R
        self.P_residual = self.H.dot(self.P).dot(self.H.T) + self.R

    def propagate(self, y_tilde):
        # integrate velocity for distance traveled
        self.distance_traveled = self.distance_traveled + y_tilde[0] * self.dt

        # propagate estimates
        self.P = self.__rk4_P(self.P, self.x_hat, y_tilde)
        self.x_hat = self.__rk4_x(self.x_hat, y_tilde)
        self.P_residual = self.H.dot(self.P).dot(self.H.T) + self.R
        return self.x_hat, self.P, self.P_residual

    def update(self, z_tilde):
        # calculate measurement matrix H
        self.H = np.array([[1, 0, -self.gps_ref * sin(self.x_hat[2]), 0, 0],
                           [0, 1, self.gps_ref * cos(self.x_hat[2]), 0, 0]])

        # calculate residual covariance for residual monitoring
        self.P_residual = self.H.dot(self.P).dot(self.H.T) + self.R

        # calculate the residual
        estimated_measurement = self.x_hat[0:2] + rotation_mat(self.x_hat[2, 0]).dot(np.array([[self.gps_ref], [0]]))
        residual = z_tilde - estimated_measurement

        # compute Kalman Gain: K = P_*H_'*(H_*P_*H_'+R_)^-1;
        self.K = self.P.dot(self.H.T).dot(
            np.linalg.inv(self.H.dot(self.P).dot(self.H.T) + self.R))

        # compute new covariance: (I-K*H)*P*(I-K*H)'+K*R*K'
        self.P = (self.I - self.K.dot(self.H)).dot(self.P).dot((self.I - self.K.dot(self.H)).T) \
                 + self.K.dot(self.R).dot(self.K.T)

        # compute new estimate
        del_x_hat = self.K.dot(residual)
        self.x_hat = self.x_hat + del_x_hat

        return self.x_hat, self.P, residual, self.P_residual

    def __x_hat_dot(self, x_hat, y_tilde):
        """
        :param x_hat: propagation state
        :param y_tilde: continuous measurement
        :return:  new state [ẋ₁ ẋ₂ ψ ḃv ḃΦ]'
        ┌    ┐   ┌                    ┐
        │ ẋ₁ │   │    (ṽ-bv)⋅cos(ψ)   │  x position
        │ ẋ₂ │   │    (ṽ-bv)⋅sin(ψ)   │  y position
        │ ψ  │ = │ (ṽ-bv)/L⋅tan(Φ-bΦ) │  heading
        │ ḃv │   │      -1/τv⋅bv      │  velocity bias
        │ ḃΦ │   │      -1/τv⋅bΦ      │  steer angle bias
        └    ┘   └                    ┘
        """
        x_dot_hat = np.array(
            [[(y_tilde[0] - x_hat[3, 0]) * cos(x_hat[2, 0])],
             [(y_tilde[0] - x_hat[3, 0]) * sin(x_hat[2, 0])],
             [(y_tilde[0] - x_hat[3, 0]) / self.L * tan(y_tilde[1] - x_hat[4, 0])],
             [-1 / self.tau_v * x_hat[3, 0]],
             [-1 / self.tau_phi * x_hat[4, 0]]])
        return x_dot_hat

    def __P_dot(self, P, x_hat, y_tilde):
        # calculate F
        F23 = 1 / self.L * (y_tilde[1] - x_hat[4, 0])
        F = array([[0, 0, -(y_tilde[0] - x_hat[3, 0]) * sin(x_hat[2, 0]), -cos(x_hat[2, 0]), 0],
                   [0, 0, (y_tilde[0] - x_hat[3, 0]) * cos(x_hat[2, 0]), -sin(x_hat[2, 0]), 0],
                   [0, 0, 0, F23, -(y_tilde[0] - x_hat[3, 0]) / self.L],
                   [0, 0, 0, -1 / self.tau_v, 0],
                   [0, 0, 0, 0, -1 / self.tau_phi]])

        # calculate G
        G20 = 1 / self.L * (-y_tilde[1] + x_hat[4, 0])
        G = array([[-cos(x_hat[2, 0]), 0, 0, 0],
                   [-sin(x_hat[2, 0]), 0, 0, 0],
                   [G20, 1 / self.L * (-y_tilde[0] + x_hat[3, 0]), 0, 0],
                   [0, 0, 1, 0],
                   [0, 0, 0, 1]])

        # F*P + P*F' + G*Q*G'
        P_dot = F.dot(P) + P.dot(F.T) + G.dot(self.Q).dot(G.T)
        return P_dot

    def __rk4_x(self, x_old, y_tilde):
        # runge kutta 45
        h = self.dt
        hh = h / 2.
        h6 = h / 6.

        y = x_old
        dy_dx = self.__x_hat_dot(y, y_tilde)
        yt = y + hh * dy_dx

        dyt = self.__x_hat_dot(yt, y_tilde)
        yt = y + hh * dyt

        dym = self.__x_hat_dot(yt, y_tilde)
        yt = y + h * dym
        dym = dyt + dym

        dyt = self.__x_hat_dot(yt, y_tilde)
        return y + h6 * (dy_dx + dyt + 2 * dym)

    def __rk4_P(self, P, x_hat, y_tilde):
        # runge kutta 45
        h = self.dt
        hh = h / 2.
        h6 = h / 6.

        y = P
        dy_dx = self.__P_dot(y, x_hat, y_tilde)
        yt = y + hh * dy_dx

        dyt = self.__P_dot(yt, x_hat, y_tilde)
        yt = y + hh * dyt

        dym = self.__P_dot(yt, x_hat, y_tilde)
        yt = y + h * dym
        dym = dyt + dym

        dyt = self.__P_dot(yt, x_hat, y_tilde)
        return y + h6 * (dy_dx + dyt + 2 * dym)


if __name__ == '__main__':
    main()
