import numpy as np
import matplotlib.pyplot as plt
import time
from utils import calc_resistive_accel
from Parameters import Parameters
from VehicleSim import VehicleSim
from SensorSuite import SensorSuite
from KalmanFilter import KalmanFilter
from MonteCarlo import MonteSim
from ControllerVelocity import VeclocityController
from PathPlanner import PathPlanner
from BatteryModel import BatteryModel


def main():
    params = Parameters()
    sensors = SensorSuite(params)
    sim = VehicleSim(params)
    monte = MonteSim(params)
    observer = KalmanFilter(sensors, params)
    path_planner = PathPlanner(params)
    v_controller = VeclocityController(params)
    battery = BatteryModel("../data/charging_coils.csv", params)

    watt_hour_motor_usage = np.array([])
    watt_hour_charging = np.array([])
    u = np.array([0.0, 0.0])

    v_controller.trajectory = path_planner.get_velocity_trajectory()

    for k in range(len(monte.time)):
        v = sim.x[3, 0]
        v_hat = sensors.get_velocity(sim.x)

        # # update path planner route if neccesarry
        # path_updated = path_planner.update_controller_trajectory(sim.x)
        #
        # if path_updated is True:
        #     v_controller.set_trajectory(path_planner.get_velocity_trajectory())

        # calculate v_controller input
        u[0] = v_controller.calc_input(v_hat)

        # update state of charge due to motor losses and regenerative charging
        usage = battery.update_battery_loss(v, u[0], 0, params.dt)
        watt_hour_motor_usage = np.append(watt_hour_motor_usage, usage)

        # update battery state of charge after dynamic charging
        charging = battery.update_dynamic_charging(sim.x[0, 0], sim.x[1, 0])
        watt_hour_charging = np.append(watt_hour_charging, charging)

        # add resistive acceleration to vehicle input
        u[0] = u[0] - np.sign(v) * calc_resistive_accel(v, params)

        # propogate vehcile forward
        x = sim.propagate(u)

        if k == round(10 / monte.dt):
            print(x[3, 0])

        # obtain measurments from current state
        y_tilde = [v_hat, sensors.get_steering_angle(x)]

        # propagate the estimated state and covariance
        x_hat, P, resid_P = observer.propagate(y_tilde)

        # discrete update
        if np.mod(k, round(1 / (float(params.gps_freq) * params.dt))) == 0:
            x_hat, P, resid, resid_P = observer.update(sensors.get_gps_pos(x))

        monte.x[:, :, k] = x
        monte.x_hat[:, :, k] = x_hat
        monte.P[:, :, k] = P
        monte.resid_P[:, :, k] = resid_P
        monte.u[:, k] = u

    used_wH = battery.max_battery_capacity - battery.state_of_charge
    print("Total wH: " + str(used_wH))
    print("Estimated Average wH/km: " + str(used_wH / observer.distance_traveled * 1000))

    # plt.step(monte.time, watt_hour_charging)
    plt.plot(monte.time, monte.x[3, 0])
    # plt.plot(monte.time, monte.u[0])
    # plt.plot(monte.x[0, 0], monte.x[1, 0])
    plt.show()


if __name__ == '__main__':
    main()
