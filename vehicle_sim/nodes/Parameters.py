# coding=utf-8
from numpy import deg2rad, pi, random


class Parameters:
    def __init__(self):
        # simulator
        self.x0 = [0, 0, 0, 0, 0]  # [x y ψ v Φ]
        self.simulations = 2
        self.end_time = 10  # seconds
        self.dt = 0.01  # integration time step
        self.noise = True  # turns sensor noise on/off (True|False)
        self.seed = random.seed(1)

        # velocity sensor
        self.v_sig = 1  # [m/s] after <v_sig_time_des>
        self.v_nominal = 10.  # [m/s] used for sizing Q
        self.v_sig_time_des = 20.  # time desired for v_sig
        self.tau_v = 100.  # time constant for sensor bias
        self.v_b_sig_ss = 0.1  # steady state bias
        self.velocity_sensor_freq = 100.

        # steering angle sensor
        self.phi_sig = deg2rad(1)  # rad
        self.phi_sig_time_des = 20.
        self.tau_phi = 100.  # time constant for sensor bias
        self.phi_b_sig_ss = deg2rad(2)  # steady state bias
        self.steering_angle_sensor_freq = 100.

        # gps sensor
        self.gps_x_sig = .03  # meters
        self.gps_y_sig = .03  # meters
        self.gps_freq = 2  # hz
        self.gps_samp = 1 / float(self.gps_freq)
        self.gps_distance_from_back_axel = .25  # meters

        # vehicle
        self.wheel_base = 2.64922  # meters
        self.wheel_track = 1.5  # meters (measured in Blender)
        self.max_steer_angle = deg2rad(30)  # phi
        self.max_change_steer_angle = deg2rad(10)  # per second
        self.max_velocity_limit = 40  # m/s
        self.max_accel_limit = 5  # m/s^2
        self.wheel_radius = 0.3  # meters

        # initial state covariance
        self.x1_sig_init = self.gps_x_sig
        self.x2_sig_init = self.gps_y_sig
        self.psi_sig_init = deg2rad(0)  # unknown
        self.v_b_sig_init = self.v_b_sig_ss
        self.phi_b_sig_init = self.phi_b_sig_ss

        # velocity controller controller and path planner
        self.v_error_epsilon = 1  # recalculate velocity path if error > epsilon
        self.ds = 0.01  # point distances for location
        self.nominal_control_speed = 5  # 60 mph = 26.8224
        self.time_to_nominal = 1  # sec

        # steering angle controller and path planner
        self.steering_resistance = 1  # N s/radians
        self.control_reference = 1  # from back axel reference point

        # vehicle battery
        self.max_battery_capacity = 1000  # Wh
        self.initial_charge = 1  # percent charge
        self.charging_coil_power_transfer = 60  # kW

        # resistive force parameters (ford focus)
        self.resistive_force_parameters = True  # turns resistive force on/off (True|False). applies only to ROS
        self.front_area = 2.24546551  # m^2
        self.drag_coefficient = 0.274
        self.mass = 1643.819  # kilograms (3624 lbs)
        self.mass_factor = 1.05
        self.tire_pressure = 2.41317  # bar 35 psi
        self.air_density = 1.225  # air density as sea level kg/m^2
        self.driving_incline = deg2rad(0)  # incline of road--negative is downhill

        # ros frequency settings
        self.pub_ekf_freq = float(1/self.dt)
        self.pub_controller_freq = float(1/self.dt)
        self.pub_planner_freq = float(1/self.dt)
        self.pub_vehicle_marker_freq = 100
        self.pub_vehicle_sim_freq = float(1/self.dt)
        self.pub_battery_freq = float(1/self.dt)

        # # resistive force parameters (example car)
        # self.resistive_force_parameters = True  # turns resistive force on/off (True|False). applies only to ROS
        # self.front_area = 2  # m^2
        # self.mass = 1360  # kg - weight is
        # self.mass_factor = 1.05
        # self.drag_coefficient = 0.5
        # self.tire_pressure = 2.41317  # bar - 35 psi
        # self.air_density = 1.225  # kg/m^2 - air density as sea level
        # self.driving_incline = deg2rad(0)
