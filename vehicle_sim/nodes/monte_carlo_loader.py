#! /usr/bin/env python
# coding=utf-8

import matplotlib.pyplot as plt
import pickle


def main():
    # load monte carlo
    monte = pickle.load(open("./simulations/sims100-len50-stamp1120115528", "rb"))
    monte.subplot_monte_carlo()
    plt.show()


if __name__ == '__main__':
    main()
