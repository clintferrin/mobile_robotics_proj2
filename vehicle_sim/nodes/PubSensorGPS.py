#! /usr/bin/env python

import Parameters
import SensorSuite as ss
import rospy
from numpy import array
from vehicle_sim.msg import bicycle_state
from geometry_msgs.msg import PoseStamped


def main():
    params = Parameters.Parameters()
    sensors = ss.SensorSuite(params)
    gps_measurement = PubSensorGPS(sensors)
    # gps_measurement.publish(hz=params.gps_freq)
    gps_measurement.publish(hz=params.gps_freq)
    # phi = sensors.get_steering_angle(x)

    # spin() simply keeps python from exiting until this node is stopped


class PubSensorGPS:
    def __init__(self, sensors):
        self.sensors = sensors
        self.x = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        self.gps_pos = PoseStamped()
        rospy.init_node('z_tilde', anonymous=True)
        rospy.Subscriber("/x_state", bicycle_state, self.__callback)

    def __callback(self, data):
        self.x[0, 0] = data.x
        self.x[1, 0] = data.y
        self.x[2, 0] = data.psi

    def publish(self, hz=2):
        pub = rospy.Publisher('z_tilde', PoseStamped, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        while not rospy.is_shutdown():
            z_tilde = self.sensors.get_gps_pos(self.x).copy()
            self.gps_pos.header.stamp = rospy.Time.now()
            self.gps_pos.pose.position.x = z_tilde[0, 0]
            self.gps_pos.pose.position.y = z_tilde[1, 0]
            # print("x pos: " + str(self.gps_pos.pose.position.x))
            # log_str = "z_tilde: " + str(z_tilde)
            # rospy.loginfo(log_str)

            pub.publish(self.gps_pos)
            rate.sleep()


if __name__ == '__main__':
    main()
