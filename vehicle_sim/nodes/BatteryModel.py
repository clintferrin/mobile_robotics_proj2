#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
import Parameters as pm
from VehicleSim import VehicleSim
import numpy as np
import pandas as pd
import utils as ut


def main():
    params = pm.Parameters()
    path_to_charging_coil_list = "../data/charging_coils.csv"
    battery = BatteryModel(path_to_charging_coil_list, params)
    battery.update_dynamic_charging(0, 0)



class BatteryModel:
    def __init__(self, path_to_charging_coil_list, params):
        self.max_battery_capacity = params.max_battery_capacity
        self.state_of_charge = params.initial_charge * self.max_battery_capacity
        self.dt = params.dt
        self.coils = []

        # dynamic charging parameters
        self.charging_coil_file_path = path_to_charging_coil_list

        if self.charging_coil_file_path is not None:
            self.coils = self.parse_coil_csv_file(self.charging_coil_file_path)

        # batery loss parameters
        self.__meters_per_sec_to_km_per_hour = 60 * 60 / float(1000)
        self.__watt_seconds_to_watt_hours = 1 / float(60.0 * 60.0)
        self.__g = 9.80665
        self.__p = params.tire_pressure
        self.__theta = params.driving_incline
        self.__M = params.mass
        self.__rho = params.air_density
        self.__Cd = params.drag_coefficient
        self.__A = params.front_area
        self.__f_m = params.mass_factor

    def parse_coil_csv_file(self, file_path):
        data_numpy = pd.read_csv(self.charging_coil_file_path).values
        coils = []
        for coil in data_numpy:
            # calculate Q
            args = {"name": coil[0],
                    "x_pos": float(coil[1]),
                    "y_pos": float(coil[2]),
                    "angle": float(coil[3]),
                    "x_charge_radius": float(coil[4]),
                    "y_charge_radius": float(coil[5]),
                    "partial_charge_width": float(coil[6]),
                    "max_power_transfer": float(coil[7])}

            coils.append(self.__ChargingCoil(args))
        return coils

    def update_battery_loss(self, v_hat, accel, steering, dt):
        power = self.__calc_power(v_hat, accel - np.sign(v_hat) * self.__calc_resistive_accel(v_hat))
        watt_hours = power * dt * self.__watt_seconds_to_watt_hours
        self.__update_state_of_charge(watt_hours)
        return watt_hours

    def update_dynamic_charging(self, x, y):
        pos_vec = np.array([[x], [y]], dtype=float)

        charging_coil = None
        for idx, coil in enumerate(self.coils):
            coil_center = np.array([[coil.x_pos], [coil.y_pos]])
            distance = np.linalg.norm(pos_vec - coil_center)
            if distance < coil.circle_radius:
                charging_coil = coil
                break

        if charging_coil is None:
            return 0

        else:
            ref_vec = np.array([[charging_coil.x_pos], [charging_coil.y_pos]])
            theta = charging_coil.angle
            x_error, y_error = ut.find_2D_error_components(ref_vec, pos_vec, theta)

            power = -min(charging_coil.linear_partial_charging(abs(x_error), charging_coil.x_charge_radius),
                         charging_coil.linear_partial_charging(abs(y_error), charging_coil.y_charge_radius))

            watt_hours = power * self.dt

            self.__update_state_of_charge(watt_hours)
            return power

    def __update_state_of_charge(self, watt_hours):
        self.state_of_charge = self.state_of_charge - watt_hours

        if self.state_of_charge > self.max_battery_capacity:
            self.state_of_charge = self.state_of_charge

        elif self.state_of_charge < 0:
            self.state_of_charge = 0
        return self.state_of_charge

    def __calc_resistive_force(self, v):
        # calculate coeficient of rolling resistance
        Cr = 0.005 + 1 / self.__p * \
             (0.01 + .0095 * (v * self.__meters_per_sec_to_km_per_hour / 100) ** 2)

        # calculate all resistive forces
        rolling_resistance = self.__M * self.__g * Cr * cos(self.__theta)
        drag_resistance = 1 / 2.0 * self.__rho * self.__A * self.__Cd * v ** 2
        road_slope_resistance = self.__M * self.__g * sin(self.__theta)

        return rolling_resistance + drag_resistance + road_slope_resistance

    def __calc_resistive_accel(self, v):
        resistive_force = self.__calc_resistive_force(v)
        return resistive_force / self.__M

    def __calc_power(self, v, accel):
        # incules regenerative breaking
        resistive_power = self.__calc_resistive_force(v) * v
        acceleration_power = self.__f_m * self.__M * accel * v
        return acceleration_power + resistive_power

    class __ChargingCoil:
        def __init__(self, args):
            # referenced from center of charging pad
            self.name = args["name"]
            self.x_pos = args["x_pos"]
            self.y_pos = args["y_pos"]
            self.angle = args["angle"]
            self.x_charge_radius = args["x_charge_radius"]
            self.y_charge_radius = args["y_charge_radius"]
            self.partial_charge_width = args["partial_charge_width"]
            self.max_power_transfer = args["max_power_transfer"]

            self.charge_slope = self.max_power_transfer / self.partial_charge_width
            self.circle_radius = np.sqrt((self.x_charge_radius + self.partial_charge_width) ** 2 +
                                         (self.y_charge_radius + self.partial_charge_width) ** 2)

        def linear_partial_charging(self, error, radius):
            y_intercept = self.max_power_transfer + self.charge_slope * radius
            charge = -self.charge_slope * error + y_intercept
            if charge < 0:
                return 0
            elif charge > self.max_power_transfer:
                return self.max_power_transfer
            else:
                return charge


if __name__ == '__main__':
    main()
