#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
import Parameters as pm
import VehicleSim as ve
import numpy as np
from utils import rk4, Trajectory2D, Trajectory1D


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    steering_controller = SteeringController(params)


class SteeringController:
    def __init__(self, params):
        self.t = 0
        self.dt = params.dt
        self.moving_average = np.zeros(4)
        self.moving_average_idx = int(0)
        self.steering_angle_limit = params.steer_angle_limit
        self.turn_rate_limit = params.turn_rate_limit  # per second

        # initalize vehicle dynamics
        resistance = params.steering_resistance
        M = 1
        self.A = -resistance / float(M)
        self.B = 1 / M

        # initialize PID values
        self.integral = 0
        self.error_old = 0
        self.derivative = None

        self.Kp = 6
        self.Ki = 0.5
        self.Kd = 0

    def calc_input(self, phi, vd):
        phi = self.calc_moving_average(phi)

        error = phi - vd

        self.integral = self.integral + error * self.dt
        derivative = (error - self.error_old) / self.dt

        p_term = self.Kp * error
        i_term = self.Ki * self.integral
        d_term = self.Kd * derivative

        u = -(p_term + i_term + d_term)

        self.error_old = error
        return u

    def __prop_x_bar(self, x_bar, u, error):
        return np.array([[u],
                         [error],
                         [0],
                         [0]])

    def calc_moving_average(self, v):
        self.moving_average[self.moving_average_idx % len(self.moving_average)] = v
        self.moving_average_idx = self.moving_average_idx + 1
        return np.mean(self.moving_average)


if __name__ == '__main__':
    main()
