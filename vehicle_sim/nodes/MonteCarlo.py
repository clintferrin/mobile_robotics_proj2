#! /usr/bin/env python
# coding=utf-8

from numpy import zeros
import numpy as np
from Parameters import Parameters
from VehicleSim import VehicleSim
from BicycleController import BicycleController
import KalmanFilter as kf
from SensorSuite import SensorSuite
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import time
import pickle
import datetime
from matplotlib2tikz import save as tikz_save
import utils as ut


def main():
    # initialization
    params = Parameters()
    sensors = SensorSuite(params)
    controller = BicycleController(params)
    monte = MonteCarlo(params)
    sim = []
    observer = []

    # initialize the starting conditions for sim and observer
    for i in range(monte.num_simulations):
        # initialize simulation
        sim.append(VehicleSim(params))
        observer.append(kf.KalmanFilter(sensors, params))

    # run simulation
    for idx_sim, simulation in enumerate(monte.simulations):
        for t in range(len(simulation.time)):
            # calculate controller input
            u = controller.calc_zero_input(observer[idx_sim].x_hat)

            # propogate vehcile forward
            x = sim[idx_sim].propagate(u)

            # obtain measurments from current state
            y_tilde = [sensors.get_velocity(x),
                       sensors.get_steering_angle(x)]

            # propagate the estimated state and covariance
            x_hat, P, resid_P = observer[idx_sim].propagate(y_tilde)

            # discrete update
            if np.mod(t, round(1 / (float(params.gps_freq) * monte.dt))) == 0:
                x_hat, P, resid, resid_P = observer[idx_sim].update(sensors.get_gps_pos(x))
                simulation.resid = np.c_[simulation.resid, resid]
                simulation.resid_t = np.append(simulation.resid_t, t * monte.dt)

            # save simulation values
            simulation.x[:, :, t] = x
            simulation.x_hat[:, :, t] = x_hat
            simulation.P[:, :, t] = P
            simulation.resid_P[:, :, t] = resid_P

        # output percent complete
        monte.print_update(idx_sim)

    # calculate statistics and save output
    monte.end_routine()

    # monte.save_all_figures_tikz("tikz/", points=250)
    # monte.save_all_figures_tikz("tikz/_", points=2)
    monte.subplot_monte_carlo()
    plt.show()


class MonteCarlo:
    def __init__(self, params):
        # # plotting attributes for Latex
        # plt.rc('text', usetex=True)
        # plt.rc('font', family='serif')

        # fill simulations with zeros
        self.num_simulations = params.simulations
        self.simulation_len = params.end_time
        self.dt = params.dt  # integration time step
        self.end_time = params.end_time  # seconds
        self.time = np.arange(0, self.end_time, self.dt)
        self.error = []
        self.simulations = []
        self.error_three_sig = []
        self.residual_mean = []
        for i in range(self.num_simulations):
            self.simulations.append(MonteSim(params))
        self.start = time.time()

        # label information
        self.y_labels = ["Error $x$ Distance (m)",
                         "Error $y$ Distance (m)",
                         "Error $\Psi$ Angle (rad)",
                         "Error $b_v$ Velocity Bias (m)",
                         "Error $b_{\phi}$ Steering Angle Bias (rad)"]

    def pickle_monte_carlo_runs(self):
        stamp = datetime.datetime.now().strftime("%m%d%H%M%S")
        file_name = "simulations/sims" + str(self.num_simulations) \
                    + "-len" + str(self.end_time) \
                    + "-stamp" + stamp
        pickle.dump(self, open(file_name, "wb"))

    def load_monte_carlo(self, load_file):
        return pickle.load(open(load_file, "rb"))

    def plot_trajectory(self, idx):
        plt.figure()
        x = self.simulations[idx].x[0, 0, :]
        y = self.simulations[idx].x[1, 0, :]
        plt.subplot(2, 1, 1)
        plt.plot(x, y)

        # limit to square output
        buf = 0.5  # scaled distance around maximum point
        max_x_val = max(x)
        max_y_val = max(y)
        min_x_val = min(x)
        min_y_val = min(y)

        x_range = max_x_val - min_x_val
        y_range = max_y_val - min_y_val
        square_len = max(x_range, y_range)
        x_mid = (max_x_val + min_x_val) / 2
        y_mid = (max_y_val + min_y_val) / 2
        plt.xlim(x_mid - (square_len / 2 + square_len * buf),
                 x_mid + (square_len / 2 + square_len * buf))
        plt.ylim(y_mid - (square_len / 2 + square_len * buf),
                 y_mid + (square_len / 2 + square_len * buf))

        plt.xlabel("$x$ posistion")
        plt.ylabel("$y$ posistion")

        plt.subplot(2, 1, 2)
        x = self.simulations[idx].x_hat[0, 0, :]
        y = self.simulations[idx].x_hat[1, 0, :]
        plt.plot(x, y)
        plt.xlim(x_mid - (square_len / 2 + square_len * buf),
                 x_mid + (square_len / 2 + square_len * buf))
        plt.ylim(y_mid - (square_len / 2 + square_len * buf),
                 y_mid + (square_len / 2 + square_len * buf))

        plt.xlabel("$\hat{x}$ posistion")
        plt.ylabel("$\hat{y}$ posistion")

    def plot_trajectory_together(self, trajectory_num, points=160):
        plt.plot(ut.downsample_array(self.simulations[0].x[0, 0], points=points),
                 ut.downsample_array(self.simulations[0].x[1, 0], points=points),
                 label="$\mathbf{x}$")

        plt.plot(ut.downsample_array(self.simulations[0].x_hat[0, 0], points=points),
                 ut.downsample_array(self.simulations[0].x_hat[1, 0], points=points),
                 label="$\hat{\mathbf{x}}$")

        plt.axis('equal')

        plt.xlabel("Posistion $x$ (m)")
        plt.ylabel("Posistion $y$ (m)")

    def print_update(self, idx):
        print("%6.2f%% - Time: %.2f sec" %
              ((idx + 1) / float(self.num_simulations) * 100,
               time.time() - self.start))

    def end_routine(self):
        print("")
        print("Monte Carlo Simulations: %6d sims" % self.num_simulations)
        print("  Length of simulations: %6.2f secs" % self.simulation_len)
        print(" Total computation time: %6.2f secs" % (time.time() - self.start))
        self.calc_error()
        self.calc_three_sig()
        self.calc_residual_mean()

    def calc_error(self):
        self.error = zeros((self.num_simulations, 5, len(self.time)))
        for idx, simulation in enumerate(self.simulations):
            self.error[idx, 0, :] = simulation.x_hat[0, 0, :] - simulation.x[0, 0, :]
            self.error[idx, 1, :] = simulation.x_hat[1, 0, :] - simulation.x[1, 0, :]
            self.error[idx, 2, :] = simulation.x_hat[2, 0, :] - simulation.x[2, 0, :]
            self.error[idx, 3, :] = simulation.x_hat[3, 0, :] - simulation.x[5, 0, :]
            self.error[idx, 4, :] = simulation.x_hat[4, 0, :] - simulation.x[6, 0, :]
        return self.error

    def calc_residual_mean(self):
        self.residual_mean = zeros((self.num_simulations, 2, len(self.simulations[0].resid_t)))
        for idx, simulation in enumerate(self.simulations):
            self.residual_mean[idx, 0, :] = simulation.resid[0, :]
            self.residual_mean[idx, 1, :] = simulation.resid[1, :]
        self.residual_mean = np.mean(self.residual_mean, axis=0)
        return self.residual_mean

    def calc_three_sig(self):
        self.error_three_sig = zeros((5, len(self.time)))
        for idx in range(5):
            self.error_three_sig[idx, :] = np.std(self.error[:, idx, :], axis=0) * 3

    def plot_state(self, idx, state):
        plt.plot(self.time, self.simulations[idx].x[state, 0, :])

    def plot_state_estimate(self, idx, state):
        plt.plot(self.time, self.simulations[idx].x_hat[state, 0, :])

    def plot_state_error(self, state, points=160):
        for error in self.error[:, state, :]:
            plt.plot(ut.downsample_array(self.time, points=points),
                     ut.downsample_array(error, points=points),
                     linewidth=.5)

    def plot_P(self, state, points=160):
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(np.sqrt(self.simulations[-1].P[state, state]) * 3, points=points),
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")

        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(-np.sqrt(self.simulations[-1].P[state, state]) * 3, points=points),
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")

        # plt.legend(["Kalman Filter $3\sigma$"], loc='upper left', )

    def plot_state_error_three_sig(self, state, points=160):
        alpha = 0.2
        plt.fill_between(ut.downsample_array(self.time, points=points),
                         ut.downsample_array(self.error_three_sig[state], points=points),
                         ut.downsample_array(self.error_three_sig[state], points=points) * -1,
                         facecolor='red', alpha=alpha, step="pre",
                         label="Kalman Filter $3\sigma$")

    def plot_state_monte_carlo(self, state, points=160):
        self.plot_state_error_three_sig(state, points=points)
        self.plot_state_error(state, points=points)
        self.plot_P(state, points=points)
        plt.ylabel(self.y_labels[state])

    def subplot_monte_carlo(self, points=500):
        plt.figure()
        num_states = 5
        for idx in range(num_states):
            plt.subplot(num_states, 1, idx + 1)
            self.plot_state_monte_carlo(idx, points=points)
            plt.grid(linestyle='-', linewidth=.25)
            if idx == 0:
                plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
                           ncol=2, fancybox=True, shadow=True)
        plt.xlabel("Time [sec]")

        plt.figure()
        self.show_residual()

        plt.figure()
        plt.subplot(2, 1, 1)
        plt.step(self.simulations[0].resid_t, self.residual_mean[0])
        plt.ylabel("Ensamble Residual Mean for $x$ (m)")

        plt.subplot(2, 1, 2)
        plt.step(self.simulations[1].resid_t, self.residual_mean[1])
        plt.ylabel("Ensamble Residual Mean for $y$ (m)")
        plt.ylabel("Time (s)")

    def plot_residual_state(self, state, points=160):
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(np.sqrt(self.simulations[state].resid_P[state, state]) * 3, points=points),
                 linewidth=0.9, color="red", linestyle="--", label="Kalman Filter Residual $3\sigma$")
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(-np.sqrt(self.simulations[state].resid_P[state, state]) * 3, points=points),
                 linewidth=0.9, color="red", linestyle="--")
        plt.xlabel("Time (s)")

        for simulation in self.simulations:
            plt.scatter(simulation.resid_t, simulation.resid[state, :])

    def show_residual(self, points=160):
        plt.subplot(2, 1, 1)
        self.plot_residual_state(0, points=points)
        plt.ylabel("Residual $x$ Error Distance (m)")
        plt.legend()

        plt.subplot(2, 1, 2)
        self.plot_residual_state(1, points=points)
        plt.ylabel("Residual $y$ Error Distance (m)")
        plt.legend()

    def save_figure_pdf(self, file_name):
        plt.savefig(file_name, format='pdf')

    def save_all_figures_pdf(self):
        # pdf save directory
        pdf_trajectory = PdfPages("./pdf/trajectory.pdf")
        pdf_state_error = PdfPages("./pdf/errors.pdf")
        pdf_residuals = PdfPages("./pdf/resisuals.pdf")

        # save state error plots
        font = {'family': 'normal',
                'weight': 'bold',
                'size': 12}
        plt.rc('font', **font)

        # save individual state error plots
        plt.figure(num=None, figsize=(7.5, 4), dpi=100)
        for idx in range(5):
            self.plot_state_monte_carlo(idx)
            plt.grid(linestyle='-', linewidth=.25)
            plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
                       ncol=2, fancybox=True, shadow=True)
            self.save_figure_pdf(pdf_state_error)
            plt.clf()

        # save subplots for first three states
        plt.figure(num=None, figsize=(7.5, 8), dpi=100)
        self.subplot_monte_carlo()
        self.save_figure_pdf(pdf_state_error)
        plt.clf()

        # save true trajectory plot
        plt.figure(num=None, figsize=(6, 4.5),
                   dpi=100, facecolor='w', edgecolor='k')
        self.plot_trajectory(0)
        self.save_figure_pdf(pdf_trajectory)

        # save residual plots

        pdf_trajectory.close()
        pdf_state_error.close()
        pdf_residuals.close()

    def save_all_figures_tikz(self, path, points=160):
        # specify state limits for the error plots
        x_lim = [5, 20]

        y_lim = [[-.2, .2],
                 [-.2, .2],
                 [-.15, .15],
                 [-.25, .25],
                 [-.15, .15]]

        # save state error plots
        for idx in range(5):
            self.plot_state_monte_carlo(idx, points=points)
            plt.xlabel("Time (s)")
            plt.xlim(x_lim)
            plt.ylim(y_lim[idx])
            plt.legend(loc='upper left')
            tikz_save(path + "error_state_" + str(idx) + ".tex",
                      figurewidth='\\linewidth',
                      figureheight='.5\\linewidth')
            plt.clf()

        # save x trajectory plot
        trajectory_num = 0
        self.plot_trajectory_together(0, points=points)
        plt.legend(loc='upper left')
        tikz_save(path + "trajectory_" + str(trajectory_num) + ".tex",
                  figurewidth='.5\\linewidth',
                  figureheight='.5\\linewidth')
        plt.clf()

        # # save residual plots
        # y_lim = [-.4, .4]
        # self.plot_residual_state(0, points=points)
        # plt.ylabel("Residual $x$ Error Distance (m)")
        # plt.legend()
        # plt.xlim(x_lim)
        # plt.ylim(y_lim)
        # tikz_save(path + "residual_0.tex",
        #           figurewidth='\\linewidth',
        #           figureheight='.5\\linewidth')
        # plt.clf()
        #
        # self.plot_residual_state(1, points=points)
        # plt.ylabel("Residual $y$ Error Distance (m)")
        # plt.legend()
        # plt.xlim(x_lim)
        # plt.ylim(y_lim)
        # tikz_save(path + "residual_1.tex",
        #           figurewidth='\\linewidth',
        #           figureheight='.5\\linewidth')
        # plt.clf()

        plt.step(self.simulations[0].resid_t, self.residual_mean[0])
        plt.ylabel("Ensamble Residual Mean for $x$ (m)")
        plt.xlabel("Time (s)")
        plt.xlim(x_lim)
        tikz_save(path + "residual_mean_0.tex",
                  figurewidth='\\linewidth',
                  figureheight='.5\\linewidth')
        plt.clf()

        plt.step(self.simulations[1].resid_t, self.residual_mean[1])
        plt.ylabel("Ensamble Residual Mean for $y$ (m)")
        plt.xlabel("Time (s)")
        plt.xlim(x_lim)
        tikz_save(path + "residual_mean_1.tex",
                  figurewidth='\\linewidth',
                  figureheight='.5\\linewidth')
        plt.close()


class MonteSim:
    def __init__(self, params):
        self.num_simulations = params.simulations
        self.end_time = params.end_time  # seconds
        self.dt = params.dt  # integration time step
        self.time = np.arange(0, self.end_time, self.dt)
        self.x_dim = [7, 1]
        self.x_hat_dim = [5, 1]
        self.num_measurements = 2
        self.K_dim = [self.x_hat_dim[0], self.num_measurements]

        self.x = zeros((self.x_dim[0], self.x_dim[1], self.time.shape[-1]))
        self.x_hat = zeros((self.x_hat_dim[0], self.x_hat_dim[1], self.time.shape[-1]))
        self.P = zeros((self.x_hat_dim[0], self.x_hat_dim[0], self.time.shape[-1]))
        self.resid = np.array([[], []])
        self.resid_t = np.array([])
        self.resid_P = zeros((2, 2, self.time.shape[-1]))
        self.K = zeros((self.x_hat_dim[0], self.num_measurements, self.time.shape[-1]))
        self.u = zeros((2, self.time.shape[-1]))


if __name__ == '__main__':
    main()
