import numpy as np
import matplotlib.pyplot as plt
import time
from utils import calc_resistive_accel
from Parameters import Parameters
from VehicleSim import VehicleSim
from SensorSuite import SensorSuite
from KalmanFilter import KalmanFilter
from MonteCarlo import MonteSim
from ControllerVelocity import VeclocityController
from ControllerSteering import SteeringController
from PathPlanner import PathPlanner


def main():
    params = Parameters()
    sensors = SensorSuite(params)
    sim = VehicleSim(params)
    monte = MonteSim(params)
    observer = KalmanFilter(sensors, params)
    path_planner = PathPlanner(params)
    v_controller = VeclocityController(params)
    phi_controller = SteeringController(params)

    u = np.array([0.0, 0.0])
    v_controller.trajectory = path_planner.get_velocity_trajectory()

    for k in range(len(monte.time)):
        v = sim.x[3, 0]
        v_hat = sensors.get_velocity(sim.x)
        v_hat = v
        phi_hat = sensors.get_steering_angle(sim.x)

        u[0] = v_controller.calc_input(v_hat)
        u[1] = phi_controller.calc_input(phi_hat, .75)

        # add resistive acceleration to vehicle input
        u[0] = u[0] - np.sign(v) * calc_resistive_accel(v, params)

        # propogate vehcile forward
        x = sim.propagate(u)

        # obtain measurments from current state
        y_tilde = [v_hat, phi_hat]

        # propagate the estimated state and covariance
        x_hat, P, resid_P = observer.propagate(y_tilde)

        # discrete update
        if np.mod(k, round(1 / (float(params.gps_freq) * params.dt))) == 0:
            x_hat, P, resid, resid_P = observer.update(sensors.get_gps_pos(x))

        monte.x[:, :, k] = x
        monte.x_hat[:, :, k] = x_hat
        monte.P[:, :, k] = P
        monte.resid_P[:, :, k] = resid_P
        monte.u[:, k] = u

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    # plt.step(monte.time, watt_hour_charging)
    ax1.plot(monte.time, monte.x[3, 0])
    ax2.plot(monte.time, monte.x[4, 0])
    # plt.plot(monte.time, monte.u[0])
    # plt.plot(monte.x[0, 0], monte.x[1, 0])
    plt.show()


if __name__ == '__main__':
    main()
