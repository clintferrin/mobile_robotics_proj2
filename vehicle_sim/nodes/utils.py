from numpy import cos, sin, array, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.io

g = 9.80665


def main():
    theta = np.pi / 4.0
    pt_ref = np.array([[.5], [0]])
    plot_pose(pt_ref[0, 0], pt_ref[1, 0], theta, scale=5)
    pt_error = np.array([[2], [-0.5]])

    x_ref = np.cos(theta)
    y_ref = np.sin(theta)
    ref = np.array([[x_ref], [y_ref]], dtype=float)
    ref_perp = np.array([[-ref[1, 0]], [ref[0, 0]]], dtype=float)
    error = pt_error - pt_ref

    x_error = np.dot(error.T, ref)[0, 0]
    y_error = np.dot(error.T, ref_perp)[0, 0]
    error = np.array([[x_error], [-y_error]])
    new_pt = rotation_mat(theta).dot(error) + pt_ref

    print(x_error)
    print(y_error)
    # angle_error = np.arctan2(y_error, x_error)

    plt.scatter(pt_ref[0, 0], pt_ref[1, 0])
    plt.scatter(pt_error[0, 0], pt_error[1, 0])
    plt.scatter(new_pt[0, 0], new_pt[1, 0])
    plt.axis('equal')
    plt.show()


def rk4(fun, dt, x_old, input_1, input_2):
    # runge kutta 45
    h = dt
    hh = h / 2.
    h6 = h / 6.

    y = x_old
    dy_dx = fun(y, input_1, input_2)
    yt = y + hh * dy_dx

    dyt = fun(yt, input_1, input_2)
    yt = y + hh * dyt

    dym = fun(yt, input_1, input_2)
    yt = y + h * dym
    dym = dyt + dym

    dyt = fun(yt, input_1, input_2)
    return y + h6 * (dy_dx + dyt + 2 * dym)


def rotation_mat(theta):
    R = array([[cos(theta), -sin(theta)],
               [sin(theta), cos(theta)]])
    return R


def calc_resistive_force(v, params):
    v_km_h = v * 60 * 60 / 1000
    Cr = 0.005 + 1 / float(params.tire_pressure) * \
         (0.01 + .0095 * (v_km_h / 100) ** 2)  # rolling resistance
    return params.mass * g * Cr * cos(params.driving_incline) \
           + 1 / 2.0 * params.air_density * params.drag_coefficient * params.front_area * v ** 2 \
           + params.mass * g * sin(params.driving_incline)


def calc_resistive_accel(v, params):
    resistive_force = calc_resistive_force(v, params)
    return resistive_force / params.mass


def calc_power(v, accel, params):
    # incules regenerative breaking
    resistive_power = calc_resistive_force(v, params) * v
    acceleration_power = params.mass_factor * params.mass * accel * v
    return acceleration_power + resistive_power


def downsample_array(arr, points=500):
    step = int(round(len(array(arr)) / float(points)))
    if step < 1:
        return arr
    return arr[0:-1:step]


class Trajectory1D:
    def __init__(self, ds=.01):
        self.path = np.array([])
        self.path_dot = np.array([])
        self.path_ddot = np.array([])
        self.ds = ds


def build_path_arc(circle, start_theta, end_theta, direction, ds):
    path = Trajectory2D()
    start_theta = np.mod(start_theta, direction * np.pi * 2)
    end_theta = np.mod(end_theta, direction * np.pi * 2)

    if direction * end_theta < direction * start_theta:
        end_theta = end_theta + direction * 2 * np.pi

    num = (start_theta - end_theta) / ds
    theta = np.linspace(start_theta, end_theta, max(abs(round(num)), 1))
    # theta = np.arange(start_theta, end_theta, direction * ds)

    path.x = circle.r * np.cos(theta) + circle.x
    path.y = circle.r * np.sin(theta) + circle.y

    path.theta = wrap_to_pi(theta + direction * np.pi / 2)

    # if I include the radius of the circe in the derivative, the controller blows up.
    path.x_dot = -direction * np.sin(theta)
    path.y_dot = direction * np.cos(theta)

    path.x_ddot = -direction * np.cos(theta)
    path.y_ddot = -direction * np.sin(theta)

    path.k = np.ones(path.x.shape) * direction * 1 / float(circle.r)
    path.s_geo = abs(circle.r * (start_theta - end_theta))
    path.s = circle.r * abs(theta - start_theta)

    return path


def build_line_path(p1, p2, ds):
    path = Trajectory2D(ds=ds)
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    theta = np.arctan2(dy, dx)  # heading used to calculate derivatives
    path.s_geo = np.sqrt(dx ** 2 + dy ** 2)

    # do to numerical issues,
    if abs(dx) <= 1e-10 and abs(dy) <= 1e-10:
        path.x = np.array([])
        path.y = np.array([])

    elif abs(dx) <= 1e-10:
        path.y = np.linspace(p1[1], p2[1], max(1, np.ceil(path.s_geo / ds)), endpoint=True)
        path.y_dot = np.ones(path.y.shape)
        path.y_ddot = np.zeros(path.y.shape)

        path.x = np.squeeze(np.ones((1, len(path.y))) * p1[0])
        path.x_dot = np.zeros(path.x.shape)
        path.x_ddot = np.zeros(path.y.shape)

    else:
        m = dy / dx
        b = p1[1] - m * p1[0]
        path.x = np.linspace(p1[0], p2[0], max(1, np.ceil(path.s_geo / ds)), endpoint=True)
        path.x_dot = np.ones(path.x.shape) * np.cos(theta)
        path.x_ddot = np.zeros(path.x.shape)

        path.y = m * path.x + b
        path.y_dot = np.ones(path.y.shape) * np.sin(theta)
        path.y_ddot = np.zeros(path.y.shape)

    path.theta = np.ones(path.x.shape) * theta
    path.k = np.zeros(path.x.shape)
    path.s = np.arange(0, path.x.shape[0] * ds, ds)
    path.s = path.s[:len(path.x)]
    return path


class Trajectory2D:
    def __init__(self, ds=.001):
        self.x = np.array([])
        self.y = np.array([])
        self.x_dot = np.array([])
        self.y_dot = np.array([])
        self.x_ddot = np.array([])
        self.y_ddot = np.array([])
        self.ds = ds
        self.theta = np.array([])
        self.k = np.array([])
        self.s = np.array([])
        self.transitions = np.array([0])
        self.s_geo = 0

    def concatenate(self, new_path, self_range=None, new_path_range=None):
        """
        :param new_path: Continuous concatenation. Concatantes Trajectory2D at end of self.
                     Removes last element of self, and increments distance index to
                     by step=ds.
        :param self_range: [start,stop] does not support negative indexing
        :param new_path_range: [start,stop] does not support negative indexing
        :return: Trajectory2D
        """
        tmp_path = Trajectory2D()

        if new_path.s.size is 0:
            return self

        if self_range is None:
            self_range = [0, self.x.shape[0] - 1]
        if new_path_range is None:
            new_path_range = [0, new_path.x.shape[0]]

        if self.s.shape == (0,):
            self_range = [0, 0]
            tmp_path.s = np.r_[self.s[0:0], new_path.s[new_path_range[0]:new_path_range[1]]]
        else:
            tmp_path.s = np.r_[self.s[self_range[0]:self_range[1]],
                               new_path.s[new_path_range[0]:new_path_range[1]] + self.s[self_range[1]] - new_path.s[
                                   new_path_range[0]]]

        # save transition points
        tmp_path.transitions = np.append(self.transitions, new_path.transitions + self_range[1])

        # save new path points
        tmp_path.x = np.r_[self.x[self_range[0]:self_range[1]],
                           new_path.x[new_path_range[0]:new_path_range[1]]]
        tmp_path.x_dot = np.r_[self.x_dot[self_range[0]:self_range[1]],
                               new_path.x_dot[new_path_range[0]:new_path_range[1]]]
        tmp_path.x_ddot = np.r_[self.x_ddot[self_range[0]:self_range[1]],
                                new_path.x_ddot[new_path_range[0]:new_path_range[1]]]

        tmp_path.y = np.r_[self.y[self_range[0]:self_range[1]],
                           new_path.y[new_path_range[0]:new_path_range[1]]]
        tmp_path.y_dot = np.r_[self.y_dot[self_range[0]:self_range[1]],
                               new_path.y_dot[new_path_range[0]:new_path_range[1]]]
        tmp_path.y_ddot = np.r_[self.y_ddot[self_range[0]:self_range[1]],
                                new_path.y_ddot[new_path_range[0]:new_path_range[1]]]

        tmp_path.theta = np.r_[self.theta[self_range[0]:self_range[1]],
                               new_path.theta[new_path_range[0]:new_path_range[1]]]
        tmp_path.k = np.r_[self.k[self_range[0]:self_range[1]],
                           new_path.k[new_path_range[0]:new_path_range[1]]]
        tmp_path.s_geo = self.s_geo + new_path.s_geo

        return tmp_path

    def update_derivatives(self):
        self.x_dot = np.cos(self.theta)
        self.x_ddot = -np.sin(self.theta)
        self.y_dot = np.sin(self.theta)
        self.y_ddot = np.cos(self.theta)

    def rotate_path(self, theta):
        """
        Rotates path around index zero. Rotation includes derivatives and second derivatives
        :param theta: angle of rotation
        :return: Trajectory2D
        """
        tmp_path = Trajectory2D()
        R = rotation_mat(theta)
        pose = np.c_[self.x - self.x[0], self.y - self.y[0]].T
        pose_dot = np.c_[self.x_dot, self.y_dot].T
        pose_ddot = np.c_[self.x_ddot, self.y_ddot].T

        pose = R.dot(pose)
        pose_dot = R.dot(pose_dot)
        pose_ddot = R.dot(pose_ddot)

        tmp_path.x = pose[0, :] + self.x[0]
        tmp_path.y = pose[1, :] + self.y[0]
        tmp_path.x_dot = pose_dot[0, :]
        tmp_path.y_dot = pose_dot[1, :]
        tmp_path.x_ddot = pose_ddot[0, :]
        tmp_path.y_ddot = pose_ddot[1, :]

        tmp_path.theta = self.theta + theta
        tmp_path.k = self.k
        tmp_path.s = self.s
        tmp_path.transitions = self.transitions
        return tmp_path

    def translate_path(self, x, y):
        new_path = self
        new_path.x = new_path.x + x
        new_path.y = new_path.y + y
        return new_path

    def reverse_path(self):
        """
        :return: Trajectory2D. Reverses all trajectory arrays and
                removes last element of self, but increments distance index to
                by step=ds.
        """
        tmp_path = Trajectory2D()
        tmp_path = tmp_path.concatenate(self)
        tmp_path.x = self.x[::-1]
        tmp_path.x_dot = self.x_dot[::-1]
        tmp_path.x_ddot = self.x_ddot[::-1]

        tmp_path.y = self.y[::-1]
        tmp_path.y_dot = self.y_dot[::-1]
        tmp_path.y_ddot = self.y_ddot[::-1]

        tmp_path.theta = -self.theta[::-1]
        tmp_path.k = self.k[::-1]
        tmp_path.s = abs(self.s[::-1] - max(self.s))
        tmp_path.transitions = (len(self.x) - 1 - self.transitions)[::-1][:-1]
        return tmp_path

    def plot_trajectory(self, ax=None, color="black"):
        if ax is None:
            ax = plt
        ax.plot(self.x, self.y, color=color)

    def plot_trajectory_transitions(self, ax=None):
        if ax is None:
            ax = plt
        for transition in self.transitions:
            ax.scatter(self.x[transition], self.y[transition])
        ax.scatter(self.x[-1], self.y[-1])

    def plot_2D_derivative(self, pts=10, scale=1, ax=None):
        if ax is None:
            ax = plt

        arrow_len = .25 * scale
        head = .025 * scale
        draw_len = arrow_len - head

        step = int(round(len(np.array(self.x)) / float(pts)))

        for idx in np.arange(0, len(np.array(self.x)), step):
            angle = np.arctan2(self.y_dot[idx], self.x_dot[idx])
            end_arrow = rotation_mat(angle).dot([[draw_len], [0]])
            ax.arrow(self.x[idx], self.y[idx], end_arrow[0, 0], end_arrow[1, 0],
                     head_width=head, head_length=head, fc='k', ec='k', overhang=.2)

    def plot_heading(self, pts=10, scale=1, ax=None):
        if ax is None:
            ax = plt

        arrow_len = .25 * scale
        head = .025 * scale
        draw_len = arrow_len - head

        step = int(round(len(np.array(self.theta)) / float(pts)))

        for idx in np.arange(0, len(np.array(self.theta)), step):
            end_arrow = rotation_mat(self.theta[idx]).dot([[draw_len], [0]])
            ax.arrow(self.x[idx], self.y[idx], end_arrow[0, 0], end_arrow[1, 0],
                     head_width=head, head_length=head, fc='k', ec='k', overhang=.2)

    def plot_trajectory_dot(self):
        fig1, ax2 = plt.subplots()
        fig1, ax1 = plt.subplots()
        s_axis = np.arange(0, len(self.x_dot) * self.ds, self.ds)

        # plot x-trajectory
        ax2.plot(s_axis, self.x_dot, label="$\dot{x}$")
        ax2.plot(s_axis, self.x_ddot, label="$\ddot{x}$")
        ax2.set_xlabel("Path Length $(m)$")
        ax2.set_ylabel("x-values")
        ax2.legend()

        # plot y-trajectory
        ax1.plot(s_axis, self.y_dot, label="$\dot{y}$")
        ax1.plot(s_axis, self.y_ddot, label="$\ddot{y}$")
        ax1.set_xlabel("Path Length $(m)$")
        ax1.set_ylabel("y-values")
        ax1.legend()

    def export_to_matlab(self, file_path):
        """
        Exports the 2D trajectory to matlab. Note that there is no file extension
        necessary as the function appends a *.mat at the end of the file
        :param file_path: path to *.mat output
        """
        path = {'x': self.x,
                'y': self.y,
                'x_dot': self.x_dot,
                'y_dot': self.y_dot,
                'x_ddot': self.x_ddot,
                'y_ddot': self.y_ddot,
                'k': self.k,
                'theta': self.theta}
        scipy.io.savemat(file_path, mdict=path)


def plot_line(p1, p2, ax=None, color="C0", dashes=(4, 3), linewidth=.75, linestyle="--"):
    p1 = np.array(p1).squeeze()
    p2 = np.array(p2).squeeze()
    if ax is None:
        ax = plt

    ax.plot([p1[0], p2[0]], [p1[1], p2[1]],
            color=color,
            linewidth=linewidth,
            linestyle=linestyle,
            dashes=dashes)


def plot_pose(x, y, theta, scale=1, head_scale=1, ax=None, head=True):
    if ax is None:
        ax = plt

    arrow_len = .3 * scale
    if head is False:
        head = 0
    else:
        head = .025 * scale * head_scale
    draw_len = arrow_len - head

    end_arrow = rotation_mat(theta).dot([[draw_len], [0]])
    ax.arrow(x, y, end_arrow[0, 0], end_arrow[1, 0],
             head_width=head * .3, head_length=head,
             fc='k', ec='k', overhang=.1, zorder=10, linewidth=.75)
    plt.plot(x, y)


def plot_vector(v1, ax=None, color=None):
    v1 = np.array(v1).squeeze()
    if ax is None:
        if color is None:
            plt.plot([0, v1[0]], [0, v1[1]],
                     linewidth=1)
        else:
            plt.plot([0, v1[0]], [0, v1[1]],
                     color=color,
                     linewidth=1)
    else:
        if color is None:
            ax.plot([0, v1[0]], [0, v1[1]],
                    linewidth=1)
        else:
            ax.plot([0, v1[0]], [0, v1[1]],
                    color=color,
                    linewidth=1)


def find_2D_error_components(pt_ref, pt_error, theta):
    x_ref = np.cos(theta)
    y_ref = np.sin(theta)
    ref = np.array([[x_ref], [y_ref]], dtype=float)
    ref_perp = np.array([[-ref[1, 0]], [ref[0, 0]]], dtype=float)
    error = pt_error - pt_ref

    x_error = np.dot(error.T, ref)
    y_error = np.dot(error.T, ref_perp)
    return x_error[0, 0], y_error[0, 0]


class Circle:
    def __init__(self, x, y, r, index=None):
        self.x = float(x)
        self.y = float(y)
        self.r = float(r)

    def copy(self):
        return Circle(self.x, self.y, self.r)

    def plot(self, ax=None, color="C0", dashes=(4, 3), linewidth=.75, linestyle="--"):
        if ax is None:
            ax = plt
        theta = np.arange(0, 2 * np.pi, .01)
        x = self.r * np.cos(theta) + self.x
        y = self.r * np.sin(theta) + self.y
        ax.plot(x, y, color=color,
                linewidth=linewidth,
                linestyle=linestyle,
                dashes=dashes
                )

    def plot_pt(self, theta, ax=None, color="black"):
        if ax is None:
            ax = plt
        ax.scatter(self.x + abs(self.r) * np.cos(theta), self.y + abs(self.r) * np.sin(theta))

    def plot_radius(self, theta, ax=None, dashes=(4, 3), linewidth=.75, linestyle="--"):
        if ax is None:
            ax = plt
        end_pt = rotation_mat(theta).dot(np.array([[self.r], [0]])) + np.array([[self.x], [self.y]])
        plot_line([self.x, self.y], end_pt, ax=ax, dashes=(4, 3), linewidth=.75, linestyle="--")


def wrap_to_pi(theta):
    return np.mod(theta + np.pi, 2 * np.pi) - np.pi


class Pose2D:
    def __init__(self, x, y, theta):
        self.x = x
        self.y = y
        self.theta = theta

    def plot(self, scale=1, ax=None, head=True):
        if ax is None:
            ax = plt

        arrow_len = .25 * scale
        if head is False:
            head = 0
        else:
            head = .025 * scale
        draw_len = arrow_len - head

        end_arrow = rotation_mat(self.theta).dot([[draw_len], [0]])
        ax.arrow(self.x, self.y, end_arrow[0, 0], end_arrow[1, 0],
                 head_width=head, head_length=head, fc='k', ec='k', overhang=.2)
        plt.plot(self.x, self.y)


def ensure_angle_wrapping(start_angle, end_angle, direction):
    if direction * end_angle < direction * start_angle:
        end_angle = end_angle + direction * 2 * np.pi
    return end_angle - start_angle


def y_eps_path_builder(path, eps):
    print("y_eps_path_builder")
    new_path = Trajectory2D()
    y_d = np.array([[path.x], [path.y]])
    y_eps = y_d + eps * np.array([[np.cos(path.theta)],
                                  [np.sin(path.theta)]])
    new_path.x = y_eps[0, 0, :]
    new_path.y = y_eps[1, 0, :]
    new_path.theta = path.theta
    new_path.s = path.s
    new_path.k = path.k

    for idx in range(len(new_path.x)):
        # find circle center and radius of curvature point
        if abs(path.k[idx]) < 1e-10:
            continue

        if idx == path.transitions[4]:
            break_point = 1

        r_inner = 1 / float(path.k[idx])
        R = r_inner * np.array([[np.cos(path.theta[idx] + np.pi / 2.0)], [np.sin(path.theta[idx] + np.pi / 2.0)]])
        center = R + np.array([[path.x[idx]], [path.y[idx]]])

        # find radius of new point
        r_outter = np.sqrt(eps ** 2 + r_inner ** 2)
        new_path.k[idx] = np.sign(path.k[idx]) * 1 / float(r_outter)
        pt_diff = y_eps[:, 0, [idx]] - center
        angle = np.arctan2(pt_diff[1, 0], pt_diff[0, 0])
        new_path.theta[idx] = angle + np.sign(path.k[idx]) * np.pi / 2.0

    new_path.ds = path.ds
    new_path.transitions = path.transitions
    new_path.s_geo = path.s_geo
    new_path.update_derivatives()
    return new_path


if __name__ == '__main__':
    main()
