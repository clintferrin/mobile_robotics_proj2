function x_dot = acker_x_prop(x,a_bar,input2,params)
    x_dot = [x(4)*cos(x(3))
             x(4)*sin(x(3))
             x(4)/params.L*tan(x(5)) 
             a_bar(1) 
             a_bar(2)];
end
