Ensure the following dependencies are met by running
```bash
sudo apt install -y python-pip python-dev
sudo pip install scipy numpy pandas 
```

To run the output, do the following
1. Ensure the packages copied to the "src" folder
2. Build catkin workspace
3. Source the devel/source.sh file
4. Launch the following launch file:
```bash
roslaunch ackermann_drive_teleop manual_input.launch
```